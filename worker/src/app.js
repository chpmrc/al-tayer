const MicroserviceKit = require('microservice-kit');
const EsClient = require('./esclient');

const microserviceKit = new MicroserviceKit({
  type: 'consumer',
  config: null,
  amqp: {
    url: 'amqp://broker',
    queues: [
      {
        key: 'search',
        name: 'search',
        options: { durable: false }
      }
    ],
    logger: function() {
      var args = Array.prototype.slice.call(arguments);
      args.unshift('[amqpkit]');
      console.log.apply(console, args);
    }
  }
});

microserviceKit.init()
  .then(() => {
    console.log('MSKit initialized');
    const searchQueue = microserviceKit.amqpKit.getQueue('search');
    // `done` is provided by the framework and bound to resolve/reject
    searchQueue.consumeEvent('search', (payload, done, progress) => {
      const {query} = payload;
      EsClient.search(query)
        .then((data) => done(null, data))
        .catch((err) => done(err));
    });
  })
  .catch(console.error);
