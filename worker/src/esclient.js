const elasticsearch = require('elasticsearch');
const queryBuilder = require('./query-builder');

/**
 * Singleton to share connection to ES. 
 * @class EsClient
 */
class EsClient {

  /**
   * Initializes the main ES client.
   * @memberOf EsClient
   */
  constructor() {
    this.client = new elasticsearch.Client({
      host: 'search:9200',
      log: 'info',
    });
  }

  /**
   * Perform the search with the given keyword.
   * @param {String} keyword
   * @return {any} The search results
   */
  search(keyword) {
    const query = queryBuilder.buildQuery(keyword);
    return this.client.search({ index: 'products', body: query })
  };

}


module.exports = new EsClient();
