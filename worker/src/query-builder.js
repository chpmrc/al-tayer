/**
 * Build a query, with the given keyword, that does the following:
 * - Performs exact match with `sku` and `ediRef`.
 * - Performs fuzzy match with `name` and `description`.
 * - De-boosts out-of-stock products.
 * @param {String} keyword The keyword
 * @return {String} The query
 */
function buildQuery(keyword) {
  let body = {
    from: 0,
    // Size increased to increase the chance of showing out-of-stock products at the bottom
    size: 50,
    query: {
      bool: {
        should: [{
          // sku OR ediRef should be an exact match
          term: {
            sku: keyword
          },
        }, {
          term: {
            ediRef: keyword
          },
        }, {
          // name OR description should be a fuzzy match
          match: {
            name: {
              query: keyword,
              fuzziness: 2,
            }
          },
        }, {
          match: {
            description: {
              query: keyword,
              fuzziness: 2,
            },
          }
        }]
      },
    },
    // out-of-stock products should be at the bottom
    sort: [{
      isInStock: 'desc'
    }],
  };

  return body;
}

module.exports = {buildQuery};