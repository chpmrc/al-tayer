#!/bin/sh
set -e

if [ "$1" = "" ]; then
    until nc -z -v -w5 broker:5672; do
      echo "Broker unreachable, retrying..."
      sleep 1;
    done
    until nc -z -v -w5 search:9200; do
      echo "Search unreachable, retrying..."
      sleep 1;
    done
    echo "Indexing products (automatically retrying if ES not ready)"
    ./bin/build-index -f ./data/products.json
    exec "npm" "start"
fi

exec "$@"