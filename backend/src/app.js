const express = require('express');
const cors = require('cors');
const router = require('./router');
const Searcher = require('./searcher');
const app = express();

app.use(cors());
app.use(router);

Searcher.init()
  .then(() => {
    app.listen(3000, (err) => {
      if (!err) {
        console.log('Server started');
      } else {
        console.error('Error: ', err);
        throw err;
      }
    });
  });

// See http://eng.wealthfront.com/2016/11/03/handling-unhandledrejections-in-node-and-the-browser/
process.on('unhandledRejection', (reason) => {
  console.error(reason);
  process.exit(1);
});