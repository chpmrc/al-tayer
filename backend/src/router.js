const express = require('express');
const router = express.Router();
const Searcher = require('./searcher');

router.get('/search/quick', (req, res) => {
  // How to avoid init every time without polling at startup?
  const keyword = req.query.keyword;
  if (!keyword) {
    res.status(400);
    res.json({ error: 'Missing keyword' });
  } else {
    Searcher.search(keyword)
      .then((results) => {
        res.status(200);
        res.json(results)
      })
      .catch((error) => {
        res.status(500);
        res.json({ error });
      });
  }
});

module.exports = router;
