const MicroserviceKit = require('microservice-kit');

/**
 * A singleton to share the connection to the broker.
 * Initialization is explicit (constructor is pure).
 * @class Searcher
 */
class Searcher {
  constructor() {
    this.client = new MicroserviceKit({
      type: 'producer',
      config: null,
      amqp: {
        url: 'amqp://broker',
        queues: [
          {
            key: 'search',
            name: 'search',
            options: { durable: false }
          }
        ]
      },
      logger: function() {
        var args = Array.prototype.slice.call(arguments);
        args.unshift('[amqpkit]');
        console.log.apply(console, args);
      }
    });
  }

  init() {
    return this.client.init()
      .then(() => {
        this.queue = this.client.amqpKit.getQueue('search');
      });
  }

  search(keyword) {
    return this.queue.sendEvent('search', { query: keyword });
  }
}

module.exports = new Searcher();