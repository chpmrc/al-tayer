#!/bin/sh
set -e

if [ "$1" = "" ]; then
    exec "npm" "start"
fi

exec "$@"