# Al Tayer case study

## System requirements

- Docker >= 1.12.3
- docker-compose >= 1.8.1

## Setup

We use `docker-compose up` to deploy the following containers:

- `search`: The Elastic Search server.
- `broker`: A RabbitMQ server to handle messages between containers.
- `worker`: A node app that consumes and responds to search events by querying the ES server.
- `backend`: HTTP API that acts as a middleman between the `frontend` and the `worker` by producing search events for the `worker`.
- `frontend`: The client (a React app served via the simple `http-server`).

You can see the main application at `http://${HOSTNAME}:5000` once deployed.

## Notes

- The `search` container can take up to a few minutes to bootstrap. The backend will keep trying to connect to the ES server automatically. So don't panic if the search takes a while to work.
- I used `create-react-script` which includes webpack and related scripts for minifying and bundling JS, CSS and assets, instead of manually writing Gulp/Grunt tasks. The only additional step is to process SCSS files.

## Known Issues / TODO

- Configuration is mostly hard coded (e.g. hosts, ports, microservice-kit config etc.). That's ok as long as we use Docker.
- Sometimes ES returns weird results (e.g. `armadi` shows no results but `armad` does).