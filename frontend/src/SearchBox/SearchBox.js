import React, { Component } from 'react';
import './SearchBox.css';
import Sprite from '../Sprite/Sprite';
import Loader from '../Loader/Loader';
import settings from '../settings';

// This should be configurable
const API_ROOT = settings.API_SERVER + settings.API_PATH;

class SearchBox extends Component {
  constructor(props) {
    super(props);
    this.placeholder = "Hello, I'm looking for...";
    this.state = {
      validInput: false,
      results: null,
      windowWidth: window.outerWidth
    }
    // Bind scope
    this.handleChange = this.handleChange.bind(this);
    // Hold request's reference to allow stopping it
    this.lastRequest = null;
  }

  isSmallScreen() {
    return this.state.windowWidth < 960;
  }

  getResults(keyword) {
    const xhr = new XMLHttpRequest();
    xhr.onreadystatechange = this.setResults.bind(this);
    xhr.open('GET', `${API_ROOT}?keyword=${keyword}`, true);
    xhr.send();
    this.lastRequest = xhr;
  }

  setResults() {
    // Scoped to 
    const req = this.lastRequest;
    if (req && req.readyState === 4 && req.status === 200) {
      const data = JSON.parse(req.response);
      const {hits} = data;
      this.setState({
        // Extract actual object from search result
        results: (hits.total > 0) ? hits.hits.map((item) => item._source) : []
      });
    }
  }

  handleChange(e) {
    const value = e.target.value;
    const validInput = value.length >= 3;
    // Stop last request
    this.lastRequest && this.lastRequest.abort();
    this.setState({ results: null });
    if (validInput) {
      this.getResults(value);
    }
    this.setState({ validInput });
  }

  renderResults() {
    const validInput = this.state.validInput;
    const results = this.state.results;
    const loading = validInput && !results;
    const thereAreResults = !!results && results.length > 0;
    let toRender = null;
    
    if (validInput && thereAreResults) {
      // Render all the results
      toRender = (
        <div className="results">
          <ul>
            {results.map((item, index) => (
              <li key={index}>
                {item.name}
                {(item.isInStock)? '' : ' 🚫 '}
              </li>
            ))}
          </ul>
        </div>
      );
    }
    if (!thereAreResults) {
      // Either because the request is still loading or there are no results
      toRender = (loading)? (
        <div className="results">
          <Loader />
        </div>
      ) : null;
    }

    return toRender;  
  }

  render() {
    return (
      <div className="SearchBox">

        <div className="search">
          <input type="text" placeholder={this.placeholder} onChange={(this.isSmallScreen())? null : this.handleChange} />
          <Sprite name="search"/>
        </div>

        {(this.isSmallScreen())? null : this.renderResults()}
      </div>
    );
  }

  componentDidMount() {
    window.onresize = () => this.setState({windowWidth: window.outerWidth});
  }
}

export default SearchBox;