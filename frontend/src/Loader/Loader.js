import React from 'react';
import loader from './loader.gif'

function Loader() {
  return (
    <div className="Loader">
      <img src={loader} alt=""/>
    </div>
  )
}

export default Loader;