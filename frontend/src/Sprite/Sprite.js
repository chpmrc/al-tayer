import React from 'react';
import './sprites/sprite.css';

function Sprite({name}) {
  return (
    <div className={`icon-${name}`} />
  );
}

export default Sprite;