import React, { Component } from 'react';
import './App.css';

import '../node_modules/slick-carousel/slick/slick.css';
import '../node_modules/slick-carousel/slick/slick-theme.css';
import '../node_modules/slick-carousel/slick/slick.min.js';

import Logo from './Logo/Logo.js';
import ServiceMenu from './ServiceMenu/ServiceMenu';
import NavigationMenu from './NavigationMenu/NavigationMenu';
import SearchBox from './SearchBox/SearchBox';
import PhotoAlbum from './PhotoAlbum/PhotoAlbum';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div>
          <Logo />
          <ServiceMenu />
          <NavigationMenu />
          <SearchBox />
          <PhotoAlbum />
        </div>
      </div>
    );
  }
}

export default App;
