import React from 'react';
import './NavigationMenu.css';
import Sprite from '../Sprite/Sprite';
// import hamburger from './hamburger.png';

function NavigationMenu() {
  const items = [{
    url: '#',
    value: 'clothing'
  }, {
    url: '#',
    value: 'travel'
  }, {
    url: '#',
    value: 'nursery furniture'
  }, {
    url: '#',
    value: 'nursery interiors'
  }, {
    url: '#',
    value: 'playtime'
  }, {
    url: '#',
    value: 'bathtime'
  }, {
    url: '#',
    value: 'feeding'
  }, {
    url: '#',
    value: 'gifts'
  }]

  return (
    <div className="NavigationMenu">
      <a href="#" className="hamburger">
        <Sprite name="menu" />
      </a>
      <nav className="full">
        <ul>
          {items.map((item, index) => (
            <li key={index}>
              <a href={item.url}>{item.value.toUpperCase()}</a>
            </li>
          ))}
        </ul>
      </nav>
    </div>
  )
}

export default NavigationMenu;