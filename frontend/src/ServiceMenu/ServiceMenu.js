import React from 'react';
import './ServiceMenu.css';
import Sprite from '../Sprite/Sprite';

function ServiceMenu() {
  // Mock the purchases
  const purchasedItemCount = 2;

  const items = [{
    url: '#',
    value: 'Sign in/Register',
    class: 'signing'
  }, {
    url: '#',
    value: 'Stores/Stockists',
    class: 'stockists'
  }, {
    url: '#',
    value: 'Stores',
    class: 'stores'
  }, {
    url: '#',
    value: `Your bag (${purchasedItemCount})`,
    class: 'purchases'
  }];

  return (
    <div className="ServiceMenu">
      <a href="#" className="bag">
        <Sprite name="bag" />
      </a>
      <nav className="full">
        <ul>
          {items.map((item, index) => (
            <li className={item.class} key={index} >
              <a href={item.url}>{item.value}</a>
            </li>
          ))}
        </ul>
      </nav>
    </div>
  );
};

export default ServiceMenu;