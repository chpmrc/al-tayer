import React from 'react';
import Slider from 'react-slick';

import './PhotoAlbum.css';

function PhotoAlbum() {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  };
  const items = new Array(5).fill();
  const width = window.outerWidth;
  const height = 400;

  return (
    <div className="PhotoAlbum">
      <Slider {...settings} style={{ background: '#419be0' }}>
        {items.map((item, index) => (
          <div key={index}><img src={`http://lorempixel.com/${width}/${height}/fashion/?${index}`} alt="" /></div>
        ))}
      </Slider>
    </div>
  );
}

export default PhotoAlbum;