import React from 'react';
import './Logo.css';
import Sprite from '../Sprite/Sprite';

function Logo() {
  return (
    <div className="Logo">
      <a href="#">
        <Sprite name="logo" />
      </a>
    </div>
  );
}

export default Logo;