#!/bin/sh
set -e

if [ "$1" = "" ]; then
    cd ./build
    echo "Serving from $(pwd)"
    exec "http-server" "-p" "3000" "${@}"
fi

exec "$@"